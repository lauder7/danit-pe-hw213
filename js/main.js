let deImgs = document.querySelectorAll(".image-to-show");
let nActiveImg = 0;
for (de of deImgs) de.style.display = "none";
deImgs[nActiveImg].style.display = "block";


let deCountDown = document.createElement('p');
document.body.append(deCountDown);


let deStop = document.createElement('button');
deStop.innerText = "Pause";
deStop.addEventListener("click", hndPause)
document.body.append(deStop);


let deResume = document.createElement('button');
deResume.innerText = "Resume";
deResume.addEventListener("click", hndResume)
document.body.append(deResume);


let tidTicker;
let nTickerTime = 5000;
let nTickerProgress = 0;
let nTickerRemain = nTickerTime;
let nTickerStep = 20;


let nFadeTime = 1000;


hndResume();


// The end of the main function





function hndResume() {
    tidTicker = setInterval(hndTicker, nTickerStep);
    deResume.disabled = true;
    deStop.disabled = false;
}


function hndPause() {
    clearInterval(tidTicker);
    deResume.disabled = false;
    deStop.disabled = true;
    deCountDown.innerText = deCountDown.innerText + ` [paused]`;
}


function hndTicker() {
    deCountDown.innerText = `The next picture will be in ${(nTickerRemain)/1000} sec`;

    if (nTickerProgress <= nFadeTime)
        deImgs[nActiveImg].style.opacity = nTickerProgress/nFadeTime;

    if (nTickerRemain <= nFadeTime) 
        deImgs[nActiveImg].style.opacity = nTickerRemain/nFadeTime;
    
    nTickerProgress = (nTickerProgress + nTickerStep) % nTickerTime;
    nTickerRemain = nTickerTime - nTickerProgress;

    if (!nTickerProgress) {
        deImgs[nActiveImg].style.display = "none";
        nActiveImg = (nActiveImg + 1) % deImgs.length;
        deImgs[nActiveImg].style.opacity = 0;
        deImgs[nActiveImg].style.display = "block";
    }
}
